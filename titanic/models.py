from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import cross_val_score
import pandas as pd
import titanic.preprocessing as pre

def logistic(df, model_cols):
    """Compute logistic regression and return the score.

    The target of the model is 'Survived' and the variables of the regression are
    selected with the list `model_cols`.

    Parameters
    ----------
    df : pandas.DataFrame
        data for the regression
    model_cols : list
        selection of DataFrame variables to include in the model

    Returns
    -------
    The score of the logistic regression.
    """
    X_train, X_test, y_train, y_test = pre.parse_model(X=df.copy(), use_columns=model_cols)
    model = LogisticRegression().fit(X_train, y_train)
    #score = model.score(X_test, y_test)
    #print(score)
    return model, X_test, y_test

def random_forest(df, model_cols):
    """Compute random forest classifier and return the score.

    The target of the model is 'Survived' and the variables of the regression are
    selected with the list `model_cols`.

    Parameters
    ----------
    df : pandas.DataFrame
        data for the regression
    model_cols : list
        selection of DataFrame variables to include in the model

    Returns
    -------
    The score of the RandomForest.
    """

    X_train, X_test, y_train, y_test = pre.parse_model(X=df.copy(), use_columns=model_cols)
    model = RandomForestClassifier(max_depth=2, random_state=0).fit(X_train, y_train)
    #score = model.score(X_test, y_test)
    #print(score)
    return model, X_test, y_test

def cross_val_model(df, model, model_cols):
    """Compute scores for a given model using Kfold cross-validation.

    Parameters
    ----------
    df : pandas.DataFrame
        data for the model
    model : sklearn model Object
        the model object to use in the cross validation
    model_cols : list
        selection of DataFrame variables to include in the model

    Returns
    -------
    The list of score for each model using Kfold method, with 5 folds.
    """

    if "Survived" not in df.columns :
        raise ValueError("target column survived should belong to df")
    y = df["Survived"]
    X = df[model_cols]
    scores = cross_val_score(model, X, y, cv=5)

    return scores

if __name__ == "__main__":
    data = pre.loadfile('titanic_train.csv')
    data = pre.input_missing_values(data)
    model_cols1 =['SibSp', 'Parch', 'Fare']
    scores = cross_val_model(data, RandomForestClassifier(max_depth=2, random_state=0), model_cols1)
    print("Accuracy: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))
