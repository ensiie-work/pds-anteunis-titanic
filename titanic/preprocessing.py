import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn import preprocessing
import os

def loadfile(title) :
    """Load the given file in a DataFrame and return it.

    The file must be in the same directory as this module.

    Parameters
    ----------
    title : str
        the name of the file, without any path or '/'

    Returns
    -------
    The DataFrame.
    """

    data = pd.read_csv(os.path.dirname(__file__)+'/'+title)
    return data

def input_missing_values(dfin):
    """Fill missing values of the given DataFrame and return a new one.

    Numeric variables are filled with median, non numeric variables are
    filled with mode.

    Parameters
    ----------
    dfin : pandas.DataFrame
        the DataFrame

    Returns
    -------
    A new DataFrame without NA value.
    """

    df = dfin.copy()
    for col in df.columns:
        if (df[col].dtype is float) or (df[col].dtype is int) or (df[col].dtype == np.dtype('float64')):
            df[col] = df[col].fillna(df[col].median())
        if (df[col].dtype == object):
            df[col] = df[col].fillna(df[col].mode()[0])

    return df

def parse_model(X, use_columns):
    """Split a DataFrame in train sets and test sets according to a list of variables.

    The target of the model is the variable 'Survived'. The DataFrame is splitted
    in test_set for target and variables, and train_set for target and variables.
    4 Dataframes are returned (X_train, X_test, y_train, y_test).

    Parameters
    ----------
    X : pandas.DataFrame
        the DataFrame to split
    use_columns : list
        list of variables to include in the splitted DataFrame

    Returns
    -------
    The tuple (X_train, X_test, y_train, y_test) corresponding to test sets and
    train sets for Variables and target.

    Raises
    ------
    ValueError :
        if 'target' is not in the DataFrame
    """

    if "Survived" not in X.columns:
        raise ValueError("target column survived should belong to df")
    target = X["Survived"]
    X = X[use_columns]
    
    return train_test_split(X, target, test_size=0.20)

def dummify_features(df):
    """
    Transform categorical variables to dummy variables.

    Parameters
    ----------
    df: dataframe containing only categorical features

    Returns
    -------
    X: new dataframe with dummified features
       Each column name becomes the previous one + the modality of the feature
    enc: the OneHotEncoder that produced X
    """

    colnames = df.columns
    le_dict = {}
    for col in colnames:
        le_dict[col] = preprocessing.LabelEncoder()
        le_dict[col].fit(df[col])
        df.loc[:, col] = le_dict[col].transform(df[col])

    enc = preprocessing.OneHotEncoder()
    enc.fit(df)
    X = enc.transform(df)

    dummy_colnames = [cv + '_' + str(modality) for cv in colnames for modality in le_dict[cv].classes_]
    
    return pd.DataFrame(X.toarray(), columns=dummy_colnames), dummy_colnames, enc

if __name__ == "__main__":
    data = loadfile('titanic_train.csv')
    data = input_missing_values(data)
    print(data.head())
